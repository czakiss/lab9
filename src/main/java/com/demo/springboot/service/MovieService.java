package com.demo.springboot.service;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.impl.MovieImpl;

public class MovieService implements MovieImpl {

    @Override
    public  MovieDto convertToMovie(CreateMovieDto createMovie, MovieListDto movies)throws NullPointerException {

        if( createMovie.getYear() == null ||
            createMovie.getTitle().isEmpty() ||
            createMovie.getImage().isEmpty()) {
            throw new NullPointerException("Empty Value");
        }
        return new MovieDto(
                movies.getLastId()+1,
                createMovie.getTitle(),
                createMovie.getYear(),
                createMovie.getImage()
        );
    }

    @Override
    public MovieDto editMovie(MovieListDto movies, Integer id, String title, Integer year, String image) throws Exception {
        String newTitle = title;
        Integer newYear = year;
        String newImage = image;

        try{
            MovieDto oldMovie = movies.getMovie(id);

            if(title == null){
                newTitle = oldMovie.getTitle();
            }

            if(year == null){
                newYear = oldMovie.getYear();
            }

            if(image == null){
                newImage = oldMovie.getImage();
            }

            return new MovieDto(id,newTitle,newYear,newImage);

        } catch (Exception e) {
            throw e;
        }
    }

}
