package com.demo.springboot.impl;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;

public interface MovieImpl {

    MovieDto convertToMovie(CreateMovieDto createMovie, MovieListDto movies);


    MovieDto editMovie(MovieListDto movies, Integer id, String title, Integer year, String image) throws Exception;
}
