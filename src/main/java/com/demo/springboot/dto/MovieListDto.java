package com.demo.springboot.dto;

import java.util.List;
import java.util.stream.Collectors;

public class MovieListDto {
    private List<MovieDto> movies;

    public MovieListDto(List<MovieDto> movies) {
        this.movies = movies;
    }

    public List<MovieDto> getMovies() {
        return movies;
    }
    public void addMovie(MovieDto movie) {
        this.movies.add(movie);
    }
    public Integer getLastId(){
        return movies.get(movies.size()-1).getMovieId();
    }

    public MovieDto getMovie(Integer id) throws Exception {
        for (MovieDto movie:movies) {
            if(movie.getMovieId() == id){
                return movie;
            }
        }
        throw new Exception("Id not found");
    }

    public void setMovie(MovieDto newMovie) throws Exception {
        int i = 0;
        boolean checked = false;
        for (MovieDto movie: this.movies) {
            if(movie.getMovieId() == newMovie.getMovieId()){
                this.movies.set(i,newMovie);
                checked = true;
                break;
            }
            i++;
        }
        if (!checked){
            throw new Exception("Id not found");
        }
    }

    @Override
    public String toString() {
        return "[" + movies.stream()
                .map(movie -> movie.getMovieId().toString())
                .collect(Collectors.joining(",")) + "]";
    }

    public void removeMovie(Integer id) throws Exception {
        movies.remove(getMovie(id));
    }
}
