package com.demo.springboot.dto;

import org.springframework.beans.factory.annotation.Autowired;

public class CreateMovieDto
{
    private String title;
    private Integer year;
    private String image;

    public CreateMovieDto() {

    }

    public String getTitle() { return title; }
    public Integer getYear() {
        return year;
    }
    public String getImage() { return image; }
}
