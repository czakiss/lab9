package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.impl.MovieImpl;
import com.demo.springboot.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieApiController {


    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    private final MovieListDto movies;
    private final MovieService movieService = new MovieService();

    public MovieApiController() {
        List<MovieDto> moviesList = new ArrayList<>();
        moviesList.add(
            new MovieDto(
                1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg"
            )
        );
        movies = new MovieListDto(moviesList);
    }

    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        LOG.info("--- get all movies: {}", movies.getMovies());
        return ResponseEntity.ok().body(movies);    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }


    @PutMapping("/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable Integer id,
                                            @RequestParam(name = "title") String title,
                                            @RequestParam(name = "year", required = false) Integer year,
                                            @RequestParam(name = "image", required = false) String image){

        try {
            LOG.info("--- id: {}", id);
            LOG.info("--- title: {}", title);
            LOG.info("--- id: {}", year);
            LOG.info("--- title: {}", image);

            movies.setMovie(movieService.editMovie(movies, id, title, year, image));

            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {

            LOG.info("--- " + e.getMessage());

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto movie) throws URISyntaxException {

        try {
            LOG.info("--- title: {}", movie.getTitle());
            LOG.info("--- year: {}", movie.getYear());
            LOG.info("--- image: {}", movie.getImage());

            movies.addMovie(movieService.convertToMovie(movie,movies));

            return new ResponseEntity<>(HttpStatus.CREATED);

        } catch (NullPointerException e){

            LOG.info("--- Empty Value");

            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/movies/{id}")
    public ResponseEntity<Void> deletePost(@PathVariable Integer id) {
        try {
            LOG.info("--- id: {}", id);

            movies.removeMovie(id);

            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {

            LOG.info("--- " + e.getMessage());

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
